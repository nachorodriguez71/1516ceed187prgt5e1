/*
 * Clase para facilitar a otras la entrada de datos
 */

package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * Fichero Util.java
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 * @date 12-dic-2015
 */
public class Util {

  public int pedirInt() throws IOException {
    
    InputStreamReader flujo = new InputStreamReader(System.in);
    BufferedReader teclado = new BufferedReader(flujo);
    String linea;
    int numero;
    
    linea=teclado.readLine();
    numero=Integer.parseInt(linea);
    
    return numero;
    
  }
  
 
  
 }
