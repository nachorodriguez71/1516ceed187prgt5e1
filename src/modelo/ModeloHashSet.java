/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Fichero ModeloHashSet.java
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 * @date 22-dic-2015
 */
public class ModeloHashSet implements IModelo{
  HashSet alumnos=new HashSet();
  HashSet grupos=new HashSet();

  @Override
  public void create(Alumno a) {
    alumnos.add(a);
  }

  @Override
  public void update(Alumno a) {
    for (Iterator it = alumnos.iterator(); it.hasNext();) {
      Alumno al = (Alumno)it.next();
      if (al.getId().equals(a.getId())) {
        al.copia(a);
      }
    }
  }

  @Override
  public void delete(Alumno a) {
    for (Iterator it = alumnos.iterator(); it.hasNext();) {
      Alumno al = (Alumno)it.next();
      if (al.getId().equals(a.getId())) {
        alumnos.remove(al);
      }
    }  
    alumnos.remove(a);
  }
  
  @Override
  public Boolean estaIdAlumno(String id) {
    Boolean esta=false;
    for (Iterator it = alumnos.iterator(); it.hasNext();) {
      Alumno al = (Alumno)it.next();
      if (al.getId().equals(id)) {
        esta=true;
      }
    }
    return esta;
  }

  @Override
  public HashSet<Alumno> reada() {
    return alumnos;
  }

  @Override
  public void create(Grupo g) {
    grupos.add(g);
  }

  @Override
  public void update(Grupo g) {
     for (Iterator it = grupos.iterator(); it.hasNext();) {
      Grupo gr = (Grupo)it.next();
      if (gr.getId().equals(g.getId())) {
        gr.setNombre(g.getNombre());
      }
    }
  }

  @Override
  public Boolean estaIdGrupo(String id) {
    Boolean esta=false;
    for (Iterator it = grupos.iterator(); it.hasNext();) {
      Grupo gl = (Grupo)it.next();
      if (gl.getId().equals(id)) {
        esta=true;
      }
    }
    return esta;
  }
  
  @Override
  public void delete(Grupo g) {
    for (Iterator it = grupos.iterator(); it.hasNext();) {
      Grupo g1 = (Grupo)it.next();
      if (g1.getId().equals(g.getId())) {
        grupos.remove(g1);
      }
    }
  }

  @Override
  public HashSet<Grupo> readg() {
    return grupos;
  }
}
