package vista;

import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
//import java.util.logging.Level;
//import java.util.logging.Logger;
import modelo.*;
        
/**
 * Fichero Vista.java
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 * @date 17-dic-2015
 */
public class VistaAlumno implements IVista<Alumno> {
  
  
  @Override
  public void mostrar(Alumno a) {
    Grupo g=a.getGrupo();
    VistaGrupo vg=new VistaGrupo();
    System.out.println("MOSTRAR ALUMNO");
    System.out.println("**************");
    System.out.println("Id: " + a.getId());
    System.out.println("Nombre: " + a.getNombre());
    System.out.println("Email: " + a.getEmail());
    System.out.println("Edad: " + a.getEdad());
    if (g!=null) {
      vg.mostrar(g);
    }
  }
  
  @Override
  public Alumno obtener() {
    Alumno alumno = new Alumno();
    Vista vista=new Vista();
    String id;
    String nombre;
    String email;
    int edad;
    
    System.out.println("OBTENER ALUMNO.");
    System.out.print("Id: ");
    id=vista.leerString();
    alumno.setId(id);
    System.out.print("Nombre: ");
    nombre=vista.leerString();
    alumno.setNombre(nombre);
    edad=obtenerEdad();
    alumno.setEdad(edad);
    email=obtenerEmail();
    alumno.setEmail(email);
    alumno.setEmail(email);
    
    return alumno;
  }
  
  public Alumno obtener(String id) {
    Alumno alumno = new Alumno();
    Vista vista=new Vista();
    String nombre;
    String email;
    int edad;
    
    System.out.println("OBTENER ALUMNO.");
    alumno.setId(id);
    System.out.print("Nombre: ");
    nombre=vista.leerString();
    alumno.setNombre(nombre);
    edad=obtenerEdad();
    alumno.setEdad(edad);
    email=obtenerEmail();
    alumno.setEmail(email);
    alumno.setEmail(email);
    
    return alumno;    
  }
  
  public void MostrarAlumnos(HashSet hs) {
    Alumno a1;
    for (Iterator it = hs.iterator(); it.hasNext();) {
      a1 = (Alumno)it.next();
      mostrar(a1);
    }
  }
  
  public String ObtenerId() {
    String id;
    Vista v=new Vista();
    System.out.print("Id:");
    id=v.leerString();
    return id;
  }
  
  private int obtenerEdad() {
    int edad;
    boolean salir;
    
    edad=0;
    Vista vista=new Vista();
    System.out.print("Edad: ");
    do {
      try {
        salir=true;
        edad=vista.leerEntero();
      } catch (Exception e) {
          vista.error("La edad es un número entero");
          salir=false;
      }
    } while (! salir);
    
    return edad;
  }

  private String obtenerEmail() {
    String email;
    boolean salir;
    String expresion="^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$";
    Pattern p=Pattern.compile(expresion);
    
    email="";
    Vista vista=new Vista();
    System.out.print("mail: ");
    do {
      try {
        salir=true;
        email=vista.leerString();
        Matcher m=p.matcher(email);
        if (!m.matches()) {
          System.err.println("Email incorrecto");
          salir=false;
        }
      } catch (Exception e) {
          vista.error("El email no es correcto");
          salir=false;
      }
    } while (! salir);
    
    return email;
  }
  
  
 
}
