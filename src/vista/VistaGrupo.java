package vista;

import java.util.HashSet;
import java.util.Iterator;
import modelo.Alumno;
import modelo.Grupo;
import util.Util;
        
/**
 * Fichero Vista.java
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 * @date 12-dic-2015
 */
public class VistaGrupo implements IVista<Grupo> {
   
  /**
   *
   * @param g
   */
  @Override
  public void mostrar(Grupo g) {
    
    System.out.println("MOSTRAR GRUPO");
    System.out.println("*************");
    System.out.println("Id: " + g.getId());
    System.out.println("Nombre: " + g.getNombre());
    
  }
  
  @Override
  public Grupo obtener() {
    Grupo g = new Grupo();
    Vista vista=new Vista();
    String id;
    String nombre;

    System.out.println("OBTENER GRUPO.");
    System.out.print("Id: ");
    id=vista.leerString();
    g.setId(id);
    System.out.print("Nombre: ");
    nombre=vista.leerString();
    g.setNombre(nombre);
        
    return g;
  }
  
   public Grupo obtener(String id) {
    Grupo g = new Grupo();
    Vista vista=new Vista();
    String nombre;

    System.out.println("OBTENER GRUPO.");
    g.setId(id);
    System.out.print("Nombre: ");
    nombre=vista.leerString();
    g.setNombre(nombre);
        
    return g;
  }
  
  public String ObtenerId() {
    String id;
    Vista v=new Vista();
    System.out.print("Id:");
    id=v.leerString();
    return id;
  }
  
  public void MostrarGrupos(HashSet hs) {
    Grupo g1;
    for (Iterator it = hs.iterator(); it.hasNext();) {
      g1 = (Grupo)it.next();
      mostrar(g1);
    }
  }
 
}
