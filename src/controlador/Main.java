/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import modelo.IModelo;
import vista.Vista;

/**
 * Fichero Main.java
 * 
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 * @date 22-dic-2015
 */
public class Main {

    public static void main(String[] args) {
      
      //Declaramos el Modelo:
      IModelo modelo=null;
      
      //Declaramos la Vista
      Vista vista=new Vista();
      
      //Declaramos y creamos un objeto controlador
      Controlador controlador= new Controlador(modelo, vista);
      
    }

}
