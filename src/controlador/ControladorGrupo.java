/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import modelo.*;
import vista.*;

/**
 * Fichero ControladorGrupo.java
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 * @date 10-ene-2016
 */
public class ControladorGrupo {
  
  private IModelo modelo;
  //private IVista vistagrupo;
  private Vista vista;
  private VistaGrupo vistagrupo=new VistaGrupo();
  private Grupo g1;
  
  ControladorGrupo(IModelo modelo, Vista vista) {
    this.modelo=modelo;
    this.vista=vista;
    inicio();
  }
  
  private void inicio() {
    String opcion;
    String id;
    
    do {
      vista.menuCrud("GRUPO");
      opcion=vista.leerString();
      switch (opcion) {
        case "c" :
          g1 = vistagrupo.obtener();
          modelo.create(g1);
          break;
        case "r" :
          vistagrupo.MostrarGrupos(modelo.readg());
          break;
        case "u" :
          id=vistagrupo.ObtenerId();
          if (modelo.estaIdGrupo(id)) {
            g1=vistagrupo.obtener(id);
            modelo.update(g1);
          }
          break;
        case "d" :
          id=vistagrupo.ObtenerId();
          if (modelo.estaIdGrupo(id)) {
            g1.setId(id);
            modelo.delete(g1);
          }
          break;
      }
    } while (!opcion.equals("e"));
  }
}
