package controlador;

import modelo.*;
import vista.*;

/**
 * Fichero Controlador.java
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 * @date 13-dic-2015
 */
public class Controlador {

    //private Alumno alumno;
    //private Grupo grupo;
    
    private IModelo modelo;
    //private IModelo<ModeloHashSet> modelohashset;
    private Vista vista;
    //Configurador configurador; // Patron Singleton

    IVista<Grupo> vistagrupo;  // Vista parametrizada
    IVista<Alumno> vistaaalumno; // Vista parametrizada

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }

    Controlador(IModelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        //configurar();
        //configurar();
        //inicializaObjetos();
        inicio(); 

    }

    public void inicializaObjetos() {
        Alumno alumno=new Alumno();
        Grupo grupo = new Grupo();
        
        grupo.setId("0");
        grupo.setNombre("Grupo0");
        
        alumno.setId("0");
        alumno.setNombre("Paco");
        alumno.setEdad(25);
        alumno.setEmail("paco.aldarias@ceedcv.es");
        
        alumno.setGrupo(grupo);
        
        modelo.create(alumno);
        modelo.create(grupo);

    }
    
    public void inicio() {
      String opcion;
      Boolean salir=false;
      
      do {
        vista.menuEstructura();
        opcion=vista.leerString();
        switch (opcion) {
          case "v":
            //el modelo a usar será el ModeloVector
            vector();
            salir=true; //Para que sólo lo haga una vez
            break;
          case "h":
            //el modelo a usar será el ModeloHashSet
            hashset();
            salir=true; //Para que sólo lo haga una vez
        }
      } while (!opcion.equals("e") && !salir);
    }

    private void vector() {
        
      String opcion;
        
        do {
          vista.menuModelo("VECTOR");
          modelo=new ModeloVector();
          inicializaObjetos();
          opcion=vista.leerString();
          switch (opcion) {
            case "a":
              //Alumno
              //IVista<Alumno> vistaalumno=new VistaAlumno();
              //Declaramos y creamos un objeto ControladorAlumno
              ControladorAlumno controladora= new ControladorAlumno(modelo, vista);
              break;
            case "g":
              //Grupo
              //IVista<Grupo> vistagrupo=new VistaGrupo();
              //Declaramos y creamos un objeto ControladorGrupo
              ControladorGrupo controladorg= new ControladorGrupo(modelo, vista);
              break;
          }
        } while (!opcion.equals("e"));
    }

    private void hashset() {

        String opcion;
        
        do {
          vista.menuModelo("HASHSET");
          modelo=new ModeloHashSet();
          inicializaObjetos();
          opcion=vista.leerString();
          switch (opcion) {
            case "a":
              //Alumno
              //IVista<Alumno> vistaalumno=new VistaAlumno();
              //Declaramos y creamos un objeto ControladorAlumno
              ControladorAlumno controladora= new ControladorAlumno(modelo, vista);
              break;
            case "g":
              //Grupo
              //IVista<Grupo> vistagrupo=new VistaGrupo();
              //Declaramos y creamos un objeto ControladorGrupo
              ControladorGrupo controladorg= new ControladorGrupo(modelo, vista);
              break;
          }
        } while (!opcion.equals("e"));
    }



}
