/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import java.util.*; //quitar luego, dejar sólo HashSet
import vista.*; //quitar luego


/**
 * Fichero ModeloVector.java
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 * @date 22-dic-2015
 */
public class ModeloVector implements IModelo{
  Alumno alumnos[]=new Alumno[100];
  //int tallaA=0;
  Grupo grupos[]=new Grupo[100];
  //int tallaG=0;
  
  @Override
  public void create(Alumno a) {
    //Añadimos un alumno al vector en el primer hueco que hayamos
    int i=0;
    boolean salir=false;
    
    while (!salir) {
      if (alumnos[i]==null) {
        //hemos encontrado un hueco así que introducimos el alumno
        //en ese hueco y ya podemos salir
        alumnos[i]=a;
        //tallaA++;
        salir=true; 
      }
      i++;
      if (i==alumnos.length) {
        //hemos llegado al final del vector sin encontrarnos ningún
        //hueco o acabamos de insertar el alumno en el último hueco
        if (!salir) {
          //Esto significa que el vector esta lleno y no hemos podido
          //añadir el elemento. Si quisiéramos podríamos devolver
          //algún valor especial indicándolo. En esta implementación
          //no hacemos nada. Lo unico es poner salir a true para 
          //finalizar el bucle
          salir=true;
        }
      }
    }
  }

  @Override
  public void update(Alumno a) {
    for (Alumno al : alumnos) {
      if (al!=null) {
        if (al.getId().equals(a.getId())) {
          al.copia(a); //Copia todos los datos de a en al
        }
      }
    }
  }

  @Override
  public void delete(Alumno a) {
    for (int i=0; i<alumnos.length; i++) {
      if (alumnos[i]!=null) {
        if (alumnos[i].getId().equals(a.getId())) {
          alumnos[i]=null; //Crea un hueco en el vector
          //tallaA--;
        }
      }
    }
  }
  
  @Override
  public Boolean estaIdAlumno(String id) {
    Boolean esta=false;
    for (Alumno al : alumnos) {
      if (al!=null) {
        if (al.getId().equals(id)) {
          esta=true;
        }
      }
    }  
    return esta;
  }

  @Override
  public HashSet<Alumno> reada() {
    HashSet ha = new HashSet();
   
    for (Alumno a1 : alumnos) {
      if (a1!=null) {
        ha.add(a1);
      }
    }
    return ha;
  }

  @Override
  public void create(Grupo g) {
    //Añadimos un grupo al vector en el primer hueco que hayamos
    int i=0;
    boolean salir=false;
    
    while (!salir) {
      if (grupos[i]==null) {
        //hemos encontrado un hueco así que introducimos el grupo
        //en ese hueco y ya podemos salir
        grupos[i]=g;
        salir=true; 
      }
      i++;
      if (i==grupos.length) {
        //hemos llegado al final del vector sin encontrarnos ningún
        //hueco o acabamos de insertar el grupo en el último hueco
        if (!salir) {
          //Esto significa que el vector esta lleno y no hemos podido
          //añadir el elemento. Si quisiéramos podríamos devolver
          //algún valor especial indicándolo. En esta implementación
          //no hacemos nada. Lo unico es poner salir a true para 
          //finalizar el bucle
          salir=true;
        }
      }
    }
  }

  @Override
  public void update(Grupo g) {
    for (Grupo gr : grupos) {
      if (gr!=null) {
        if (gr.getId().equals(g.getId())) {
          gr.setNombre(g.getNombre()); //Copia los datos de g en gl 
        }
      }
    }
  }

  @Override
  public Boolean estaIdGrupo(String id) {
    Boolean esta=false;
    for (Grupo gl : grupos) {
      if (gl!=null) {
        if (gl.getId().equals(id)) {
          esta=true;
        }
      }
    }  
    return esta;
  }
  
  @Override
  public void delete(Grupo g) {
    for (int i=0; i<grupos.length; i++) {
      if (grupos[i]!=null) {
        if (grupos[i].getId().equals(g.getId())) {
          grupos[i]=null; //Crea un hueco en el vector
        }
      }
    }
  }

  @Override
  public HashSet<Grupo> readg() {
    HashSet hg = new HashSet();
    
    for (Grupo gr : grupos) {
      if (gr!=null) {
        hg.add(gr);
      }
    }
    return hg;
  }
  
  public static void main (String[] args) {
    Alumno a1;
    Grupo g1;
    ModeloVector mv = new ModeloVector();
    VistaAlumno va = new VistaAlumno();
    VistaGrupo vg = new VistaGrupo();
    HashSet hs;
    
    g1 = vg.obtener();
    mv.create(g1);
    for (int i=1; i<6 ; i++) {
      a1 = va.obtener();
      a1.setGrupo(g1);
      mv.create(a1);
    }
    a1=new Alumno();
    a1.setId("2");
    mv.delete(a1);
    a1.setId("4");
    mv.delete(a1);
    a1=new Alumno();
    a1.setId("7");
    a1.setNombre("7");
    a1.setGrupo(g1);
    mv.create(a1);
    a1=new Alumno();
    a1.setId("9");
    a1.setNombre("9");
    a1.setGrupo(g1);
    mv.create(a1);
    hs=mv.reada();
    for (Iterator it = hs.iterator(); it.hasNext();) {
      a1 = (Alumno)it.next();
      va.mostrar(a1);
    }
    
    
  }
}
