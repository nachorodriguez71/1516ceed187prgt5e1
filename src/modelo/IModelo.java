/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import java.util.HashSet;

/**
  * Fichero Alumno.java
 * 
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 * @date 22-dic-2015
 */
public interface IModelo {
 //Alumno
  public void create(Alumno a);  //Crea un nuevo elemento
  public void update(Alumno a);  //Actualiza un elemento
  public void delete(Alumno a); //Borra un elemento
  public HashSet<Alumno> reada(); //Obtiene todos los alumnos
  public Boolean estaIdAlumno(String id);
  
  //Grupo
  public void create(Grupo g);
  public void update(Grupo g);  //Actualiza un elemento
  public void delete(Grupo g); //Borra un elemento
  public HashSet<Grupo> readg(); //Obtiene todos los grupos
  public Boolean estaIdGrupo(String id);
}
