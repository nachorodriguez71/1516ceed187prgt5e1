/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import modelo.*;
import vista.*;

/**
 * Fichero ControladorAlumno.java
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 * @date 10-ene-2016
 */
public class ControladorAlumno {

  private IModelo modelo;
  //private IVista vistaalumno;
  private Vista vista;
  private VistaAlumno vistaalumno= new VistaAlumno();
  private Alumno a1;
  
  ControladorAlumno(IModelo modelo, Vista vista) {
    this.modelo=modelo;
    this.vista=vista;
    inicio();
  }
  
  private void inicio() {
    String opcion;
    String id;
    
    do {
      vista.menuCrud("ALUMNO");
      opcion=vista.leerString();
      switch (opcion) {
        case "c" :
          a1 = vistaalumno.obtener();
          modelo.create(a1);
          break;
        case "r" :
          vistaalumno.MostrarAlumnos(modelo.reada());
          break;
        case "u" :
          id=vistaalumno.ObtenerId();
          if (modelo.estaIdAlumno(id)) {
            a1=vistaalumno.obtener(id);
            modelo.update(a1);
          }
          break;
        case "d" :
          id=vistaalumno.ObtenerId();
          if (modelo.estaIdAlumno(id)) {
            
            a1.setId(id);
            modelo.delete(a1);
          }
          break;
      }
    } while (!opcion.equals("e"));
  }

  
}
