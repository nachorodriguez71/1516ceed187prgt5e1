/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

/**
 * Fichero Alumno.java
 * 
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 * @date 17-dic-2015
 */
public class Alumno {
  
  /* Clase Alumno */
  private String id;
  private String nombre;
  private String email;
  private int edad;
  private Grupo grupo;
  
  public Alumno() {
    id="";
    nombre="";
    email="";
    edad=0;
    grupo=null;
   }
  
  public Alumno (String id, String nombre, String email, int edad) {
    this.id=id;
    this.nombre=nombre;
    this.email=email;
    this.edad=edad;
  }

  /**
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  /**
   * @return the grupo
   */
  public Grupo getGrupo() {
    return grupo;
  }

  /**
   * @param grupo the grupo to set
   */
  public void setGrupo(Grupo grupo) {
    this.grupo = grupo;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the edad
   */
  public int getEdad() {
    return edad;
  }

  /**
   * @param edad the edad to set
   */
  public void setEdad(int edad) {
    this.edad = edad;
  }

  public void copia(Alumno a) {
    this.nombre=a.nombre;
    this.edad=a.edad;
    this.email=a.email;
    this.grupo=a.grupo;
  }
 

  
}
