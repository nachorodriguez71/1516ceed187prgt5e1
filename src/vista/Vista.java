/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Fichero Vista.java
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 * @date 17-dic-2015
 */
public class Vista {
    
  public void menuEstructura() {
    System.out.println("MENU ESTRUCTURA");
    System.out.println("e. exit");
    System.out.println("v. vector");
    System.out.println("h. hashset");
    System.out.print("Opcion: ");
   }
  
  public void menuModelo(String clase) {
    if (clase!="") {
      System.out.println(clase);
    }
    System.out.println("MENU MODELO");
    System.out.println("e. exit");
    System.out.println("a. alumno");
    System.out.println("g. grupo");
    System.out.print("Opcion: ");
  }

  public void menuCrud(String clase) {
    if (clase!="") {
      System.out.println(clase);
    }
    System.out.println("MENU CRUD");
    System.out.println("e. exit");
    System.out.println("c. create");
    System.out.println("r. read");
    System.out.println("u. update");
    System.out.println("d. delete");
    System.out.print("Opcion: ");
  }
  
  public String leerString() {
      
    InputStreamReader flujo = new InputStreamReader(System.in);
    BufferedReader teclado = new BufferedReader(flujo);
    String linea=null;
    
    try {
      linea=teclado.readLine();
    } catch (Exception e) {
    }

    return linea;
    
  }
  
  public int leerEntero() throws IOException {
    
    InputStreamReader flujo = new InputStreamReader(System.in);
    BufferedReader teclado = new BufferedReader(flujo);
    String linea;
    int numero;
    
    linea=teclado.readLine();
    numero=Integer.parseInt(linea);
    
    return numero;
    
  }

  public void error(String error) {
      System.err.println(error);
  }

  public void mensaje(String men) {
      System.out.println(men);
  }

          
}
